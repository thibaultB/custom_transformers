import pytest
import pandas as pd
from datetime import datetime, timedelta


@pytest.fixture
def data():
    df = pd.DataFrame([[1, 2, 3, 7, 8, 9, 5, 6], [4, 5, 6, 4, 7, 8, 9, 6]]).T

    date = datetime(2020, 2, 19, 12, 0, 0)

    date_columns = [
        date,
        date + timedelta(hours=10),
        date + timedelta(days=2),
        date + timedelta(minutes=2),
        date + timedelta(minutes=30),
        date + timedelta(minutes=42),
        date + timedelta(minutes=42),
        date + timedelta(minutes=5) + timedelta(hours=5),
        date + timedelta(minutes=23) + timedelta(hours=45),
    ]
    df.loc[:, "date"] = pd.Series(date_columns)
    return df


@pytest.fixture
def dataset():
    import pandas as pd
    train = pd.DataFrame([["a", "b", "c"], [1, 2, 3]]).T
    train.columns = ["letter", "number"]

    test = pd.DataFrame([["a", "d", "c"], [1, 2, 5]]).T
    test.columns = ["letter", "number"]

    return (train, test)
