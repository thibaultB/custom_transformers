# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 11:06:37 2019

@author: thibault
"""

import pytest
from src import custom_transformers
import pandas as pd

pd.options.display.max_columns = 999


def test_SimilarColumns(dataset):
    train, test = dataset

    simcol = custom_transformers.SimilarColumns()

    simcol.fit(train)
    test = simcol.transform(test)

    assert all([a == b for a, b in zip(train, test)])


def test_DateFeatureGeneration(data):
    print(data)
    date_generator = custom_transformers.DateFeatureGeneration(col_date="date")
    res = date_generator.transform(data)

    print(list(res.columns))

    expected_columns = [
        0, 1, 'date', 'date_hr_sin', 'date_hr_cos', 'date_mnth_sin',
        'date_mnth_cos', 'date_month_2.0', 'date_month_nan',
        'date_weekday_2.0', 'date_weekday_4.0', 'date_weekday_nan',
        'date_hour_12.0', 'date_hour_17.0', 'date_hour_22.0', 'date_hour_nan',
        'date_year_2020.0', 'date_year_nan', 'date_day_19.0', 'date_day_21.0',
        'date_day_nan', 'date_is_end_month', 'date_is_start_month',
        'date_is_start_quarter', 'date_is_end_quarter', 'date_is_end_year',
        'date_is_start_year', 'date_is_leap_year'
    ]

    assert list(res.columns) == expected_columns

    print(res)

    assert 1 == 25
