# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 12:33:29 2018

@author: thibault BLANC
"""
from sklearn.base import TransformerMixin, BaseEstimator, ClassifierMixin
import bs4 as BeautifulSoup
import re
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor


class SimilarColumns(BaseEstimator, TransformerMixin):
    """ Ensure that similar columns are in test and train dataset.
    If a column is in test but not in train, then the column will be created in
    train dataset with missing value everywhere.
    
    Parameters
    ----
    None

    Attributes
    ----
    after transform return pandas dataframe with same columns name 
    as in fitted dataset.
    """
    def fit(self, df, y=None, **fit_params):
        self.col = df.columns
        return self

    def transform(self, df, **transform_params):
        for col in self.col:
            if col not in df.columns:
                df[col] = np.nan
        # reorder columns
        df = df.loc[:, self.col]
        return df


class HandleWrongNegative(BaseEstimator, TransformerMixin):
    """For columns that should not accept negative value:
        if negative value is found then value become nan

    Parameters
    ----
    cols: list of column names that sould not accept negative values

    Attributes
    ----
    Return pandas dataframe with cols columns contain missing value if value are below 0
    """
    def __init__(self, cols):
        self.cols = cols

    def fit(self, df, y=None, **fit_params):
        return self

    def __is_lower_than_0__(self, x):
        return (float(x) < 0)

    def transform(self, df, **transform_params):
        for col in self.cols:
            df.loc[df.loc[:, col].map(self.__is_lower_than_0__), col] = np.nan
        return df


class IsZero(BaseEstimator, TransformerMixin):
    """Return True if photo = 0, False otherwise

    Parameters
    ----
    cols: list fo string => names of column to test if 0

    Attributes
    ----
    Return pandas dataframe with new dummy column added (boolean),
    1 if 0, 0 in any other case.
    """
    def __init__(self, cols):
        self.cols = cols

    def fit(self, df, y=None, **fit_params):
        return self

    def __is0__(self, x):
        return (int(float(x) == 0))

    def transform(self, df, **transform_params):
        for c in self.cols:
            df.loc[:, c + "_is0"] =\
                df.loc[:, c].map(self.__is0__)
        return df


class MedianYByCategories(BaseEstimator, TransformerMixin):
    """For columns that should not accept negative value:
        if negative value is found then value become nan

    Parameters
    ----
    cols: list of column names that sould not accept negative values

    Attributes
    ----
    Return pandas dataframe with cols columns contain missing value if value are below 0
    """
    def __init__(self, col):
        self.col = col

    def fit(self, df, y=None, **fit_params):
        full_df = pd.concat(
            [df.reset_index(drop=True),
             pd.Series(y).reset_index(drop=True)],
            axis=1,
            ignore_index=True)
        full_df.columns = list(df.columns) + ["y_target"]
        self.median_by_cat =\
            full_df.groupby(self.col)["y_target"].median().reset_index()
        return self

    def transform(self, df, **transform_params):
        df =\
            df.merge(self.median_by_cat, how="left", on=self.col)
        df.drop(self.col, axis=1, inplace=True)
        df = df.rename(columns={"y_target": self.col})
        return df


class OutliersToNanWithSTD(BaseEstimator, TransformerMixin):
    """ Replace outliers (values outside mean +- 2/3 * std) with a given value
    Attributes
    ----------
    cols: list of str
        input columns
    n_time_std: std range to consider for outliers (usually 2 or 3)
    impute: int, float, str or np.nan
        substitution value for outliers
    """
    def __init__(self, cols=None, n_time_std=2, impute=np.nan):
        self.cols = cols
        self.n_time_std = n_time_std
        self.impute = impute

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = list(
                df.select_dtypes(include=['int', 'float']).columns)
        self.hi_lim, self.lo_lim = {}, {}
        for c in self.cols:
            std = df.loc[:, c].std()
            mean = df.loc[:, c].mean()
            self.lo_lim[c] = mean - self.n_time_std * std
            self.hi_lim[c] = mean + self.n_time_std * std
        return self

    def transform(self, df, **transform_params):
        for c in self.cols:
            df.loc[(df[c] < self.lo_lim[c]) | (df[c] > self.hi_lim[c]),
                   c] = self.impute
        return df


class AddNumberOfMissingValueByRow(BaseEstimator, TransformerMixin):
    """Return number of missing values by row

    Parameters
    ----
    photo_colname: name of column containing number of photo

    Attributes
    ----
    Return pandas dataframe with noPhoto column added (boolean)
    """
    def fit(self, df, y=None, **fit_params):
        return self

    def __n_missing_value__(self, x):
        return (pd.isnull(x).sum())

    def transform(self, df, **transform_params):
        df.loc[:, "nMissingValue"] =\
            df.apply(self.__n_missing_value__, axis=1)
        return df


class DifBetweenCols(BaseEstimator, TransformerMixin):
    """ Create new column as difference between two columns
    Attributes
    ----------
    cols_tuple: list of string tuple (a, b) such as result will be a - b

    """
    def __init__(self, cols_tuple=None):
        self.cols_tuple = cols_tuple

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        for a, b in self.cols_tuple:
            df.loc[:, str(a) + "_MINUS_" + str(b)] =\
                pd.to_numeric(df.loc[:, a], errors='coerce') - pd.to_numeric(df.loc[:, b], errors='coerce')
        return df


class CrossFeatures(BaseEstimator, TransformerMixin):
    """ Create new column as multiplication between two columns
    Attributes
    ----------
    cols_tuple: list of string tuple (a, b) such as result will be a * b

    """
    def __init__(self, cols_tuple=None):
        self.cols_tuple = cols_tuple

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        for a, b in self.cols_tuple:
            df.loc[:, str(a) + "_MULTIPLICATED_BY_" + str(b)] =\
                pd.to_numeric(df.loc[:, a], errors='coerce') * pd.to_numeric(df.loc[:, b], errors='coerce')
        return df


class DivBetweenCols(BaseEstimator, TransformerMixin):
    """ Create new column as division between two columns
    Attributes
    ----------
    cols_tuple: list of string tuple (a, b) such as result will be a / b

    """
    def __init__(self, cols_tuple=None):
        self.cols_tuple = cols_tuple

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        for a, b in self.cols_tuple:
            df.loc[:, str(a) + "_DIVIDED_BY_" + str(b)] =\
                pd.to_numeric(df.loc[:, a], errors='coerce') / pd.to_numeric(df.loc[:, b], errors='coerce')
        return df


class keeped_variable(BaseEstimator, TransformerMixin):
    """Indicate variable to keep in dataset.
    To check that training and test set contain same variables
    """
    def __init__(self, variable_to_keep):
        self.variable_to_keep = variable_to_keep

    def transform(self, X):
        result = X[self.variable_to_keep]
        return result

    def fit(self, X, y=None):
        return self


class DropColumns(BaseEstimator, TransformerMixin):
    def __init__(self, cols='id'):
        self.cols = cols

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        to_delete = list(set(self.cols).intersection(df.columns))
        return df.drop(to_delete, axis=1, errors='ignore')


class DropDummy(BaseEstimator, TransformerMixin):
    def fit(self, df, y=None, **fit_params):
        dummy_cols = df.apply(pd.Series.nunique)
        self.dummy_cols = dummy_cols[dummy_cols < 2].index.values.tolist()
        return self

    def transform(self, df, **transform_params):
        return df.drop(self.dummy_cols, axis=1, errors='ignore')


class DropID(BaseEstimator, TransformerMixin):
    def fit(self, df, y=None, **fit_params):
        dummy_cols = df.apply(pd.Series.nunique)
        self.dummy_cols = dummy_cols[dummy_cols ==
                                     df.shape[0]].index.values.tolist()
        return self

    def transform(self, df, **transform_params):
        return df.drop(self.dummy_cols, axis=1, errors='ignore')


class Dummify(BaseEstimator, TransformerMixin):
    """ Dummify """
    def __init__(self, cols=None):
        self.cols = cols
        self.categories = {}

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = list(
                df.select_dtypes(exclude=['int', 'float']).columns)
        for c in self.cols:
            self.categories[c] = df[c].unique()
        return self

    def transform(self, df, **transform_params):
        for c in self.cols:
            for cat in self.categories[c]:
                name = c + "_" + cat
                df[name] = df[c].apply(lambda x: 1 if x == cat else 0)
            df = df.drop(c, axis=1, errors='ignore')
        return df


class NaThreshold(BaseEstimator, TransformerMixin):
    def __init__(self, threshold=50, cols=None):
        self.threshold = threshold
        self.cols = cols

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            df_na_percent = pd.DataFrame((100 * (df.select_dtypes(
                include=['int', 'float', 'int64', 'float64']).isnull().sum()) /
                                          df.shape[0]),
                                         columns=['Na_Percentage'
                                                  ]).sort('Na_Percentage',
                                                          ascending=False)
            self.cols = df_na_percent[df_na_percent.Na_Percentage >
                                      self.threshold].index.values.tolist()
        return self

    def transform(self, df, **transform_params):
        return df.drop(self.cols, axis=1, errors='ignore')


class FillNans(BaseEstimator, TransformerMixin):
    """ Fill nans in given columns according to a given strategy
    Attributes
    ----------
    cols : list of str
        Column to transform. If None all DataFrame columns are 
transformed
    strategy : str
        Nan substitution strategy.
    """
    def __init__(self, cols=None, strategy='median', value=None):
        self.cols = cols
        self.strategy = strategy
        self.value = value
        self.sub_values = {}

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = list(df.columns)
        if self.value is None:
            if self.strategy == 'median':
                self.sub_values = {c: df[c].median() for c in self.cols}
            else:
                raise ValueError('Unknown nan filling strategy:' +
                                 str(self.strategy))
        else:
            self.sub_values = {c: self.value for c in self.cols}
        return self

    def transform(self, df, **transform_params):
        for c in self.cols:
            df[c] = df[c].fillna(self.sub_values[c])
        return df


class FillCategorical(BaseEstimator, TransformerMixin):
    def __init__(self, cols=None, impute='VIDE'):
        self.impute = impute
        self.cols = cols

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = list(
                df.select_dtypes(exclude=['int', 'float']).columns)
        return self

    def transform(self, df, **transform_params):
        df[self.cols] = df[self.cols].fillna(self.impute)
        return df


class PandasToNumpy(BaseEstimator, TransformerMixin):
    """ Transform a pandas.DataFrame to a numpy.array """
    def fit(self, df, y=None, **fit_params):
        self.features = list(df.columns)
        return self

    def transform(self, df, **transform_params):
        return df.values


class ProjectCategorical(BaseEstimator, TransformerMixin):
    """
    Function to project categorical on target
    Simple way to transform categorical in float when the dataset
    have a lot of variable
    """
    def __init__(self, cols=None, target='target', fillNA=False):
        self.cols = cols
        self.target = target
        self.fillNA = fillNA

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = list(
                df.select_dtypes(exclude=['int', 'float']).columns)
        self.target_proj = {
            c: df[self.target].groupby(df[c]).mean().to_dict()
            for c in self.cols
        }
        self.defaut_proj = {c: df[self.target].mean() for c in self.cols}
        return self

    def transform(self, df, **transform_params):
        for c in self.cols:
            if self.fillNA:
                df[c] = df[c].apply(lambda x: self.target_proj[c][x]
                                    if x in self.target_proj[c] else None
                                    if pd.isnull(x) else self.defaut_proj[c])
            else:
                df[c] = df[c].apply(lambda x: self.target_proj[c][
                    x] if x in self.target_proj[c] else self.defaut_proj[c])
        return df


class GroupRareItems(BaseEstimator, TransformerMixin):
    def __init__(self, cols=None, min_occurences=5):
        self.min_occurences = min_occurences
        self.cols = cols
        self.impute = {}
        self.kept_modalities = {}

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = list(
                df.select_dtypes(exclude=['int', 'float']).columns)
        for c in self.cols:
            self.impute[c] = df[c].min() - 1 if df[c].dtype in (
                np.float64, np.int64) else '-rare_item-'
            val_counts = df[c].value_counts()
            self.kept_modalities[c] = list(
                val_counts[(val_counts > self.min_occurences)].index)
        return self

    def transform(self, df, **transform_params):
        for c in self.cols:
            df[c] = df[c].apply(lambda x: x if pd.isnull(x) or x in self.
                                kept_modalities[c] else self.impute[c])
        return df


class OutliersToNan(BaseEstimator, TransformerMixin):
    """ Replace outliers (values outside given quantiles) with a given value
    Attributes
    ----------
    cols: list of str
        input columns
    lo: float
        lower bound quantile, below this quantile entries are outliers
    hi: float
        higher bound quantile, above this quantile entries are outliers
    impute: int, float, str or np.nan
        substitution value for outliers
    """
    def __init__(self, cols=None, lo=0.01, hi=0.99, impute=np.nan):
        self.cols = cols
        self.lo = lo
        self.hi = hi
        self.impute = impute

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = list(
                df.select_dtypes(include=['int', 'float']).columns)
        self.hi_lim, self.lo_lim = {}, {}
        for c in self.cols:
            self.lo_lim[c] = df[c].quantile(self.lo)
            self.hi_lim[c] = df[c].quantile(self.hi)
        return self

    def transform(self, df, **transform_params):
        for c in self.cols:
            df.ix[(df[c] < self.lo_lim[c]) | (df[c] > self.hi_lim[c]),
                  c] = self.impute
        return df


class DropPoll(BaseEstimator, TransformerMixin):
    def __init__(self, suffix="_24", suffix_size=3):
        self.suffix = suffix
        self.suffix_size = suffix_size

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        return df.drop(
            [e for e in df.columns if e[-self.suffix_size:] == self.suffix],
            axis=1,
            errors='ignore')


class DropColPrefix(BaseEstimator, TransformerMixin):
    def __init__(self, prefix="_24", prefix_size=3):
        self.prefix = prefix
        self.prefix_size = prefix_size

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        return df.drop(
            [e for e in df.columns if e[:self.prefix_size] == self.prefix],
            axis=1,
            errors='ignore')


from random import sample


class Subsample(BaseEstimator, TransformerMixin):
    def __init__(self, target='flag_clic', subsample_nb=50000):
        self.target = target
        self.subsample_nb = subsample_nb

    def fit(self, df, y=None, **fit_params):
        self.drop_rows = sample(df[df[self.target] != 1].index.values,
                                self.subsample_nb)
        return self

    def transform(self, df, **transform_params):
        try:
            return df.drop(self.drop_rows, axis=0, errors='ignore')
        except:
            return df


class GetFeaturesNames(BaseEstimator, TransformerMixin):
    def fit(self, df, y=None, **fit_params):
        self.feature_names = df.columns
        return self

    def transform(self, df, **transform_params):
        return df


class dropCorrelated(BaseEstimator, TransformerMixin):
    """
    Return data frame with too corelated column droped
    Keep the first column when two have linear correlation 
    above threshold
    """
    def __init__(self, cols=None, threshold=0.9):
        self.cols = cols
        self.threshold = threshold
        self.keeped_cols = []

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = list(
                df.select_dtypes(include=['int', 'float']).columns)
        c = df.loc[:, self.cols].corr().abs().unstack()
        s = c[c > self.threshold]
        so = s[s.index.get_level_values(0) != s.index.get_level_values(1)]
        self.cols = list(set(so.drop_duplicates().index.get_level_values(0)))
        self.keeped_cols = list(set(df.columns).difference(self.cols))
        return self

    def transform(self, df, **transform_params):
        return df.drop(self.cols, axis=1).to_dense()


class drop_target(BaseEstimator, TransformerMixin):
    """Drop target variable
    """
    def __init__(self, target):
        self.target = target

    def transform(self, X):
        to_delete = list(set(self.target).intersection(X.columns))
        X = X.drop(to_delete, axis=1, errors='ignore')
        return X

    def fit(self, X, y=None):
        return self


class ImputeNan(BaseEstimator, TransformerMixin):
    def __init__(self, cols=None, value_for_missing="0"):
        self.cols = cols
        self.value_for_missing = value_for_missing

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df):
        df.loc[:, self.cols] = df.loc[:,
                                      self.cols].fillna(self.value_for_missing)
        return df


class CreateDep(BaseEstimator, TransformerMixin):
    def __init__(self, columns_cp='codepostal'):
        self.columns_cp = columns_cp

    def fit(self, df, y=None, **fit_params):
        return self

    def __get_first_two__(self, x):
        return x[:2]

    def transform(self, df, **transform_params):
        df.loc[:, "dep"] = df.loc[:,
                                  self.columns_cp].map(self.__get_first_two__)
        return df


class DeleteStopWord(BaseEstimator, TransformerMixin):
    """ Delete stops word in text column
    
    Parameters
    ----
    text_col: list of columns to apply stop_word deletion
    stop_word: list of word to delete
    
    """
    def __init__(self, text_col=None, stop_word=None):
        self.stop_word = stop_word
        self.text_col = text_col

    def __clean_sentence__(self, x):
        return ((' '.join(w for w in x.split()
                          if w.lower() not in self.stop_word)))

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        for col in self.text_col:
            df.loc[:, col] = df.loc[:, col].map(self.__clean_sentence__)
        return df


class Stemming(BaseEstimator, TransformerMixin):
    """ Apply stemming in text column
    
    Parameters
    ----
    text_col: list of columns to apply stop_word deletion
    stemmer: Stelmer t ouse (from nltk)
    
    """
    def __init__(self, text_col=None, stemmer=None):
        self.stemmer = stemmer
        self.text_col = text_col

    def __stem__(self, x):
        return ((' '.join(self.stemmer.stem(w) for w in x.split())))

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        for col in self.text_col:
            df.loc[:, col] = df.loc[:, col].map(self.__stem__)
        return df


class CleanText(BaseEstimator, TransformerMixin):
    """ Apply stemming in text column
    
    Parameters
    ----
    text_col: list of columns to apply stop_word deletion
    stemmer: Stelmer t ouse (from nltk)
    
    Attributes
    ----
    Return pandas dataframe with transformed text column:
        - stop word are deleted
        - stemming is applied
        - every word are converted to lowercase
    
    """
    def __init__(self, text_col=None, stemmer=None, stop_word=None):
        self.stemmer = stemmer
        self.text_col = text_col
        self.stop_word = stop_word

    def __clean_text__(self, x):
        return ((' '.join(
            self.stemmer.stem(w.lower()) for w in x.split()
            if w.lower() not in self.stop_word)))

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        for col in self.text_col:
            df.loc[:, col] = df.loc[:, col].map(self.__clean_text__)
        return df


class CosineSimilarity(BaseEstimator, ClassifierMixin):
    """Estimator for cosine similarity Input is tf-idf matrix"""
    def __init__(self, otherParam=None):
        self.otherParam = otherParam

    def fit(self, X, y=None):
        return self

    def predict(self, X, y=None):
        return ((X * X.T))


class PriceBySquareMeters(BaseEstimator, TransformerMixin):
    """For columns that should not accept negative value:
        if negative value is found then value become nan

    Parameters
    ----
    col_surface: column_name for surface
    col_groupby: column  name to groupby from
    new_col_price_by_square_meter: colum name for the new created columns

    Attributes
    ----
    Return pandas dataframe with news columns
    """
    def __init__(self,
                 col_surface,
                 col_groupby,
                 new_col_price_by_square_meter="price_by_square_meter"):
        self.col_surface = col_surface
        self.col_groupby = col_groupby
        self.new_col_price_by_square_meter = new_col_price_by_square_meter

    def fit(self, df, y=None, **fit_params):
        if "y_target" in df.columns:
            raise ValueError('Your inut contains a "y_target" columns. \
                             Please change the name of this variable')
        full_df = pd.concat(
            [df.reset_index(drop=True),
             pd.Series(y).reset_index(drop=True)],
            axis=1,
            ignore_index=True)
        full_df.columns = list(df.columns) + ["y_target"]
        sum_price =\
            full_df.groupby(self.col_groupby)["y_target"].sum().reset_index()
        sum_surface =\
            full_df.groupby(self.col_groupby)[self.col_surface].sum().reset_index()
        surface_price = sum_price.merge(sum_surface,
                                        how="outer",
                                        on=self.col_groupby)
        surface_price.loc[:, self.new_col_price_by_square_meter] =\
            surface_price.y_target / surface_price.loc[:, self.col_surface]
        self.price_by_square_meter =\
            surface_price.loc[:, [self.new_col_price_by_square_meter,
                                  self.col_groupby]]
        return self

    def transform(self, df, **transform_params):
        df =\
            df.merge(self.price_by_square_meter, how="left", on=self.col_groupby)
        return df


class NumberOfRowByValue(BaseEstimator, TransformerMixin):
    """Count number of rows for a specific categorical variable

    Parameters
    ----
    col_groupby: column  name to groupby from
    
    Attributes
    ----
    Return pandas dataframe with news column. 
    Number of rows which take this specific categorical variable in training
    """
    def __init__(self, col_groupby, new_col_name="n_row_by_category"):
        self.col_groupby = col_groupby
        self.new_col_name = new_col_name

    def fit(self, df, y=None, **fit_params):
        self.n_row =\
            df.groupby(self.col_groupby).size().reset_index()
        self.n_row.columns = [self.col_groupby, self.new_col_name]
        return self

    def transform(self, df, **transform_params):
        df =\
            df.merge(self.n_row, how="left", on=self.col_groupby)
        return df


class StdByValue(BaseEstimator, TransformerMixin):
    """Count number of rows for a specific categorical variable

    Parameters
    ----
    col_groupby: column  name to groupby from
    
    Attributes
    ----
    Return pandas dataframe with news column. 
    Number of rows which take this specific categorical variable in training
    """
    def __init__(self, col_groupby, col_value, new_col_name="std_by_category"):
        self.col_groupby = col_groupby
        self.col_value = col_value
        self.new_col_name = new_col_name

    def fit(self, df, y=None, **fit_params):
        self.std_cat =\
            df.groupby(self.col_groupby)[self.col_value].std().reset_index()
        self.std_cat.columns = [self.col_groupby, self.new_col_name]
        return self

    def transform(self, df, **transform_params):
        df =\
            df.merge(self.std_cat, how="left", on=self.col_groupby)
        return df


class TargetStdByValue(BaseEstimator, TransformerMixin):
    """Count number of rows for a specific categorical variable

    Parameters
    ----
    col_groupby: column  name to groupby from
    
    Attributes
    ----
    Return pandas dataframe with news column. 
    Number of rows which take this specific categorical variable in training
    """
    def __init__(self, col_groupby, new_col_name="y_std_by_category"):
        self.col_groupby = col_groupby
        self.new_col_name = new_col_name

    def fit(self, df, y=None, **fit_params):
        if "y_target" in df.columns:
            raise ValueError('Your inut contains a "y_target" columns. \
                             Please change the name of this variable')
        full_df = pd.concat(
            [df.reset_index(drop=True),
             pd.Series(y).reset_index(drop=True)],
            axis=1,
            ignore_index=True)
        full_df.columns = list(df.columns) + ["y_target"]
        self.std_cat =\
            full_df.groupby(self.col_groupby)["y_target"].std().reset_index()
        self.std_cat.columns = self.col_groupby + [self.new_col_name]
        return self

    def transform(self, df, **transform_params):
        df =\
            df.merge(self.std_cat, how="left", on=self.col_groupby)
        return df


class Debug(BaseEstimator, TransformerMixin):
    def transform(self, X):
        print(X.shape)
        self.shape = shape  # You can now query shape
        self.data = data  # You can now reuse data
        # what other output you want
        return X

    def fit(self, X, y=None, **fit_params):
        return self


#
#import treetaggerwrapper
#
#class LemmatizeurTreetagger(BaseEstimator, TransformerMixin):
#    """ Apply lemmatisation in text column using treetagger
#
#    Parameters
#    ----
#    tree_tagger_directory: str of the directory containing treetagger
#                           (roots of bin, lib and cmd directory)
#    taglang: language of text ("fr", "en", ...)
#    col_text: name of column containing text
#
#    """
#    def __init__(self, tree_tagger_directory=None, taglang="fr",
#                 col_text=None, keep_number=True):
#        self.tree_tagger_directory = tree_tagger_directory
#        self.taglang = taglang
#        self.col_text = col_text
#        self.keep_number = keep_number
#
#    def get_second_element(self, liste, keep_number=True):
#        if len(liste) >= 2:
#            word, pos, lemma = liste
#            if (keep_number is True) and (lemma == '@card@'):
#                return(word)
#            elif (keep_number is False) and (lemma == '@card@'):
#                return(" ")
#            else:
#                return(lemma)
#        else:
#            return(liste[0])
#
#    def treetagger_lematization(self, sentence, tagger):
#        tags = tagger.tag_text(str(sentence))
#        lem = " ".join([self.get_second_element(x.split("\t")) for x in tags])
#        return(lem)
#
#    def fit(self, df, y=None, **fit_params):
#        return self
#
#    def transform(self, df, **transform_params):
#        tagger = treetaggerwrapper.TreeTagger(TAGLANG=self.taglang,
#                                              TAGDIR=self.tree_tagger_directory)
#        for col in self.col_text:
#            df.loc[:, col] =\
#                df.loc[:, col].map(lambda x:
#                                   self.treetagger_lematization(x, tagger))
#        return df


class ReOrder(BaseEstimator, TransformerMixin):
    """Reorder columns in dataframe for timeSeries

    Parameters
    ----
    n_predict: number of steps to predict

    Attributes
    ----
    Return pandas dataframe with reordered columns.
    """
    def __init__(self, n_predict=1):
        self.n_predict = n_predict

    def fit(self, df, y=None, **fit_params):
        self.var_to_predict =\
            ['puissance1(t+' + str(i) + ')'
             for i in range(1, self.n_predict+1)]
        self.other_variable = list(set(df.columns) - set(self.var_to_predict))
        return self

    def transform(self, df, **transform_params):
        res =\
            df.loc[:, self.var_to_predict + self.other_variable]
        return res


class DateFeatureGeneration(BaseEstimator, TransformerMixin):
    """Feature Generation for time series

    Parameters
    ----
    col_date: (str) name of column with date 

    Attributes
    ----
    Return pandas dataframe with new columns generated
    """
    def __init__(self, col_date, dummify=True):
        self.col_date = col_date
        self.dummify = dummify

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        date_series = df.loc[:, self.col_date]
        month = date_series.map(lambda x: x.month)


        weekday = date_series.map(lambda x: x.weekday())
        hour = date_series.map(lambda x: x.hour)
        year = date_series.map(lambda x: x.year)
        day = date_series.map(lambda x: x.day)

        # Add cyclical feature transformation
        hr_sin = np.sin(hour * (2. * np.pi / 24))
        hr_cos = np.cos(hour * (2. * np.pi / 24))
        mnth_sin = np.sin(month * (2. * np.pi / 12))
        mnth_cos = np.cos(month * (2. * np.pi / 12))
        weekday_sin = np.sin(weekday * (2. * np.pi / 7))
        weekday_cos = np.cos(weekday * (2. * np.pi / 7))
        

        X_date = pd.concat([
            month, weekday, hour, year, day, hr_sin, hr_cos, mnth_sin, mnth_cos
        ],
                           axis=1)

        dummy_columns = [
            self.col_date + "_" + x
            for x in ["month", "weekday", "hour", "year", "day"]
        ]
        others_columns = [
            self.col_date + "_" + x
            for x in ["hr_sin", "hr_cos", "mnth_sin", "mnth_cos", "weekday_sin", "weekday_cos"]
        ]

        new_colnames = dummy_columns + others_columns

        X_date.columns = new_colnames
        if self.dummify is True:
            dummy_date = pd.get_dummies(X_date,
                                        dummy_na=True,
                                        columns=dummy_columns)
            res = pd.concat([df, dummy_date], axis=1)
        else:
            for var in X_date.columns:
                X_date.loc[:, var] = X_date.loc[:, var].astype("category")
            res = pd.concat([df, X_date], axis=1)
        # Others variable
        res.loc[:, self.col_date + "_" + "is_end_month"] =\
            date_series.dt.is_month_end
        res.loc[:, self.col_date + "_" + "is_start_month"] =\
            date_series.dt.is_month_start
        res.loc[:, self.col_date + "_" + "is_start_quarter"] =\
            date_series.dt.is_quarter_start
        res.loc[:, self.col_date + "_" + "is_end_quarter"] =\
            date_series.dt.is_quarter_end
        res.loc[:, self.col_date + "_" + "is_end_year"] =\
            date_series.dt.is_year_end
        res.loc[:, self.col_date + "_" + "is_start_year"] =\
            date_series.dt.is_year_start
        res.loc[:, self.col_date + "_" + "is_leap_year"] =\
            date_series.dt.is_leap_year
        return res


class TimeDeltaTOHours(BaseEstimator, TransformerMixin):
    """Distance to last recorded meteo

    Parameters
    ----
    col_time_delta: (str) name of column with time delta 

    Attributes
    ----
    Return pandas dataframe with new columns generated
    """
    def __init__(self, col_time_delta):
        self.col_time_delta = col_time_delta

    def fit(self, df, y=None, **fit_params):
        return self

    def timeDelta_to_hours(self, time_delta):
        return (time_delta.days * 24 + time_delta.seconds // 3600)

    def transform(self, df, **transform_params):
        hour_delta = df.loc[:,
                            self.col_time_delta].map(self.timeDelta_to_hours)
        hour_delta.name = self.col_time_delta + "_in_hours"
        return pd.concat([df, hour_delta], axis=1)


class AddLaggedVariable(BaseEstimator, TransformerMixin):
    """ Add new lagged variables
    
    Parameters
    ----
    cols: list of columns to apply
    lag_list: list => number of lag to add
    
    """
    def __init__(self, cols, lag_list=[1]):
        self.cols = cols
        self.lag_list = lag_list

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        lagged_df = pd.DataFrame()
        for lag in self.lag_list:
            temp_lagged_df = df.loc[:, self.cols].shift(lag)
            temp_lagged_df.columns = [
                x + "_lagged" + str(lag) for x in temp_lagged_df.columns
            ]
            lagged_df = pd.concat([lagged_df, temp_lagged_df], axis=1)
        return pd.concat([df, lagged_df], axis=1)


class IsMissing(BaseEstimator, TransformerMixin):
    """ Create new boolean column for each columns with 0 if not missing value and 1 if missing value 
    Attributes
    ----------
    cols: list of columns to apply the transformers (default: take all columns)

    """
    def __init__(self, cols=None):
        self.cols = cols

    def fit(self, df, y=None, **fit_params):
        if self.cols is None:
            self.cols = df.columns
        return self

    def transform(self, df, **transform_params):
        for col in self.cols:
            df.loc[:, str(col) + "_is_missing"] =\
                pd.isnull(df.loc[:, col]).astype("int")
        return df


class RollingValue(BaseEstimator, TransformerMixin):
    """ Add rolling value
    Attributes
    ----------
    cols: list of str
        input columns to compute rolling values
    windows_size: size of windows to compute rolling value
    win_types: type of window (how to weight values)
    groupby: if need to group by window by a particular column
    date_to_sort: column name containing datetime or order
    operations: list of all operations to apply
    """
    def __init__(self,
                 cols=None,
                 windows_size=2,
                 win_types=None,
                 groupby=None,
                 date_to_sort=None,
                 median=True,
                 mean=True):
        self.cols = cols
        self.windows_size = windows_size
        self.win_types = win_types
        self.groupby = groupby
        self.date_to_sort = date_to_sort
        self.median = median
        self.mean = mean

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        if self.groupby is None:
            if self.date_to_sort is not None:
                df = df.sort_values(self.date_to_sort, ascending=True)
            if self.median:
                df.loc[:, c + "_rolling_median"] =\
                    df.loc[:, c].rolling(self.windows_size, self.win_types).median()
            if self.mean:
                df.loc[:, c + "_rolling_mean"] =\
                    df.loc[:, c].rolling(self.windows_size, self.win_types).mean()
        else:
            if self.date_to_sort is not None:
                df = df.sort_values([self.groupby, self.date_to_sort],
                                    ascending=True)
            if self.median:
                temp =\
                    df.groupby(self.groupby)[self.cols].rolling(self.windows_size, self.win_types)\
                    .median().reset_index(level=self.groupby)
                temp.drop(self.groupby, inplace=True, axis=1)
                temp.columns = [c + "_rolling_median" for c in temp.columns]

                df = df.merge(temp,
                              left_index=True,
                              right_index=True,
                              how="left")
            if self.mean:
                temp =\
                    df.groupby(self.groupby)[self.cols].rolling(self.windows_size, self.win_types)\
                    .mean().reset_index(level=self.groupby)
                temp.drop(self.groupby, inplace=True, axis=1)
                temp.columns = [c + "_rolling_mean" for c in temp.columns]

                df = df.merge(temp,
                              left_index=True,
                              right_index=True,
                              how="left")

        return df


""" DOUBLONS avec GroupRareItem
class GroupSmallCategorical(BaseEstimator, TransformerMixin):
    def __init__(self, cols, threshold=10, new_category="other"):
        self.cols = cols
        self.threshold = threshold
        self.new_category = new_category
    def fit(self, df, y=None, **fit_params):
        small_categories = {}
        for col in self.cols:
          cat = list(df.loc[:, col].value_counts().loc[df.loc[:, col].value_counts() < self.threshold].index)
          small_categories[col] = cat
        return self
    def __is_small_category__(self, x):
        return(x in self.cat_list)
    def transform(self, df, **transform_params):
        for col in self.cols:
          self.cat_list = cat
          selected_row = df.loc[:, col].map(self.__is_small_category__)
          df.loc[selected_row, col] = self.new_category
        return df
"""
""" Imputation of missing value with model
"""


class ImputerRegressorWithRF(BaseEstimator, TransformerMixin):
    """Quick missing value imputation based on non-tuned rrandom Forest

    Parameters
    ----
    feature_list: list of string => feature to use to predict y
    y: str => name of the column to impute based on feature_list

    Attributes
    ----
    Return pandas dataframe with missing column in y imputed
    """
    def __init__(self, feature_list, y):
        self.feature_list = feature_list
        self.y = y
        self.rf = RandomForestRegressor(n_estimators=100, n_jobs=4)

    def fit(self, df, y=None, **fit_params):
        target = df.loc[:, self.y]
        df = df.replace([np.inf, -np.inf], np.nan)  #handle infinite
        df = df.fillna(df.median())
        df = df.fillna(-9999)  # if some value are still missing
        self.rf.fit(df.loc[:, self.feature_list], target)
        return self

    def transform(self, df, **transform_params):
        df = df.replace([np.inf, -np.inf], np.nan)  #handle infinite
        df = df.fillna(df.median())
        df = df.fillna(-9999)  # if some value are still missing
        imputed = self.rf.predict(df.loc[:, self.feature_list])
        df.loc[pd.isnull(df.loc[:, self.y]),
               self.y] = imputed[pd.isnull(df.loc[:, self.y])]
        return df


class ImputerClassifierWithRF(BaseEstimator, TransformerMixin):
    """Quick missing value imputation based on non-tuned rrandom Forest

    Parameters
    ----
    feature_list: list of string => feature to use to predict y
    y: str => name of the column to impute based on feature_list

    Attributes
    ----
    Return pandas dataframe with missing column in y imputed
    """
    def __init__(self, feature_list, y):
        self.feature_list = feature_list
        self.y = y
        self.rf = RandomForestClassifier(n_estimators=100, n_jobs=4)

    def fit(self, df, y=None, **fit_params):
        target = df.loc[:, self.y]
        df = df.replace([np.inf, -np.inf], np.nan)  #handle infinite
        df = df.fillna(df.median())
        df = df.fillna(-9999)  # if some value are still missing
        self.rf.fit(df.loc[:, self.feature_list], target)
        return self

    def transform(self, df, **transform_params):
        df = df.replace([np.inf, -np.inf], np.nan)  #handle infinite
        df = df.fillna(df.median())
        df = df.fillna(-9999)  # if some value are still missing
        imputed = self.rf.predict(df.loc[:, self.feature_list])
        df.loc[pd.isnull(df.loc[:, self.y]),
               self.y] = imputed[pd.isnull(df.loc[:, self.y])]
        return df


class CreateCrossFeatures(BaseEstimator, TransformerMixin):
    """ Create cross features (squared, *, /, +, -)
    
    Parameters
    ----
    cross_list: list of columns name to use in feature creation
    
    Attributes
    ----
    Return pandas DF with new features added
    """
    def __init__(self, cross_list=None):
        self.cross_list = cross_list

    def __create_cross_features(self, X, cross_list):
        """ Create cross features (squared, *, /, +, -)
        """
        feature = cross_list
        feature2 = cross_list
        for feat in feature:
            feature2 = [x for x in feature2 if x != feat]
            X.loc[:, feat + "_squarred"] =\
                X.loc[:, feat] * X.loc[:, feat]
            for feat2 in feature2:
                X.loc[:, feat + "_X_" + feat2] =\
                    X.loc[:, feat] * X.loc[:, feat2]
                X.loc[:, feat + "_+_" + feat2] =\
                    X.loc[:, feat] + X.loc[:, feat2]
                X.loc[:, feat + "_-_" + feat2] =\
                    X.loc[:, feat] - X.loc[:, feat2]
                X.loc[:, feat + "_div_" + feat2] =\
                    X.loc[:, feat] / X.loc[:, feat2]
                X.loc[:, feat2 + "_div_" + feat] =\
                    X.loc[:, feat2] / X.loc[:, feat]
        return (X)

    def fit(self, df, y=None, **fit_params):
        return (self)

    def transform(self, df, **transform_params):
        return (self.__create_cross_features(df, self.cross_list))


class SmallModalityAsOthers(BaseEstimator, TransformerMixin):
    """Group small frequencies modality as an other modality

    Parameters
    ----
    cols: columns name to apply modality fgroup
    threshold: int - number threshold to switch a modality to "other"

    Attributes
    ----
    Return pandas dataframe with new modality "other" for given columns
    """
    def __init__(self, cols, threshold=10, other_name="other"):
        self.cols = cols
        self.threshold = threshold
        self.other_name = other_name

    def fit(self, df, y=None, **fit_params):
        self.modality_to_others = dict()
        for col in self.cols:
            table = df.loc[:, col].value_counts().reset_index()
            self.modality_to_others[col] =\
                list(table.loc[table.loc[:, col] < self.threshold, "index"])
        return self

    def transform(self, df, **transform_params):
        for col in self.cols:
            df.loc[df.loc[:, col].map(lambda x: x in self.modality_to_others[col]),
                   col] =\
                self.other_name
        return df


class AverageValueByCategories(BaseEstimator, TransformerMixin):
    """
    Parameters
    ----
    cols: list of column names to compute mean
    by: column to groupby

    Attributes
    ----
    Return pandas dataframe wnew columns as average values
    """
    def __init__(self, col, by):
        self.col = col
        self.by = by

    def fit(self, df, y=None, **fit_params):
        self.median_by_cat =\
            df.groupby(self.by)[self.col].mean().reset_index()
        self.median_by_cat.columns =\
            [self.by] + [x + "_average_" + self.by for x in self.col]
        return self

    def transform(self, df, **transform_params):
        df =\
            df.merge(self.median_by_cat, how="left", on=self.by)
        df.drop(self.by, axis=1, inplace=True)
        # df = df.rename(columns={"y_target":self.new_col_name})
        return df


class CreateGroupByFeature(BaseEstimator, TransformerMixin):
    """
    Parameters
    ----
    cols: list of column names to compute mean
    by: column to groupby

    Attributes
    ----
    Return pandas dataframe wnew columns as average values
    """
    def __init__(self, col, by, percentile=False):
        self.col = col
        self.by = by
        self.percentile = percentile

    def fit(self, df, y=None, **fit_params):
        operations = ["min", "max", "mean", "median", "std"]
        self.median_by_cat =\
            df.groupby(self.by)[self.col].agg(operations).reset_index()
        self.median_by_cat.columns =\
            [self.by] + [x + "_" + y + "_" + self.by for x in self.col for y in operations]
        if self.percentile:
            self.percentile_table =\
                df.groupby(self.by)[self.col].quantile([0.1, 0.25, 0.75, 0.9]).reset_index()
            self.percentile_table =\
                self.percentile_table.pivot(self.by, columns="level_1")
            self.percentile_table.columns = [
                x + str(y) + "_by_" + self.by for x in self.col
                for y in [0.1, 0.25, 0.75, 0.9]
            ]
        return self

    def transform(self, df, **transform_params):
        df =\
            df.merge(self.median_by_cat, how="left", on=self.by)
        # df.drop(self.by, axis=1, inplace=True)
        if self.percentile:
            df =\
                df.merge(self.percentile_table, how="left", on=self.by)
        # df = df.rename(columns={"y_target":self.new_col_name})
        return df


class GroupByTransformFromDict(BaseEstimator, TransformerMixin):
    """ Create groupby feature based on a dict
    
    Parameters
    ----
    transformation: dict in the form of {"Feature": [np.mean, np.std, ...]}
    groupby: column to use to groupby result
    
    Returns
    ----
    Pandas DataFrame
    
    ##################### EXAMPLE  ###############################
    # Create new function to pass to agg
    def q25(x):
        return x.quantile(0.25)
    def q75(x):
        return x.quantile(0.75)
    def q90(x):
        return x.quantile(0.90)
    def q10(x):
        return x.quantile(0.10)

    VARIABLE = {
        "A": [np.min, np.max, np.std, np.median, np.size, q25, q75, q10, q90]
    }
    transformer = GroupByTransformFromDict(VARIABLE)
    transformer.transform(df)
    ##################################################################
    """
    def __init__(self, transformation, groupby="NflId"):
        self.transformation = transformation
        self.groupby = groupby

    def fit(self, df, y=None, **fit_params):
        return self

    def transform(self, df, **transform_params):
        res = pd.DataFrame()
        for k in self.transformation.keys():
            temp_res = df.groupby(self.groupby)[k].agg(self.transformation[k])
            print(temp_res.columns)
            temp_res.columns = [
                k + "_" + ''.join(col) for col in temp_res.columns.values
            ]
            res = pd.concat([res, temp_res])
        return (res)
